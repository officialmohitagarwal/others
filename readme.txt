SECTION A
TASK 1 & 2 : https://sectiona-6ynw.vercel.app/

SECTION B
TASK 1 : https://sectionbtask1.vercel.app/
TASK 2 : https://sectionbtask2.vercel.app/


SECTION C
Q2.
Suppose you are building a financial planning tool - which requires us to fetch bank statements 
from the user. Now, we would like to encrypt the data - so that nobody - not even the developer 
can view it. What would you do to solve this problem?

Solution : To ensure that the bank statements are encrypted and secure, I will use a combination of 
           encryption technologies and secure data storage practices. First we can use Use SSL/TLS 
           encryption to secure data in transit between the user's device and our server. This will 
           ensure that data is protected against interception during transmission.Like wise there are 
           so many other steps we can take such as storing the data encrypted in a secure database. 
           This will protect it against unauthorized access in case of a data breach.
           We can also use an encryption algorithm to encrypt the data, such as Advanced 
           Encryption Standard (AES).We can access controls to restrict access to the data based on 
           user roles and permissions.Implementation of monitoring and logging to track any access 
           or modifications to the data will also be heplful for the above case.

